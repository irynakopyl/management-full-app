using Management.Common.DTO;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Management.WebAPI.IntegrationTests
{
    public class ControllersTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        public ControllersTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CreateProject_WhenSuccess_StatusCode200()
        {
            ProjectDTO pr = new ProjectDTO
            {
                Name = "Aspernatur vero quas et ipsum.",
                Description = "Molestiae incidunt praesentium dolor odit culpa voluptatibus maxime et nam.\nAut nam et laudantium omnis et sed",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                AuthorId = 25,
                TeamId = 5
            };
            string proj = JsonConvert.SerializeObject(pr);
            var httpResponse = await _client.PostAsync("api/Projects", new StringContent(proj, Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CreateProject_WhenAddedOne_ThenProjectsPlusOne()
        {
            var httpResponse = await _client.GetAsync("api/Projects");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            int count = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse).Count;
            ProjectDTO pr = new ProjectDTO
            {
                Name = "Aspernatur vero quas et ipsum.",
                Description = "Molestiae incidunt praesentium dolor odit culpa voluptatibus maxime et nam.\nAut nam et laudantium omnis et sed",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                AuthorId = 5,
                TeamId = 5
            };
            string proj = JsonConvert.SerializeObject(pr);
            await _client.PostAsync("api/Projects", new StringContent(proj, Encoding.UTF8, "application/json"));
            httpResponse = await _client.GetAsync("api/Projects");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(count+1, JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse).Count);
        }

        [Fact]
        public async Task DeleteUser_WhenSuccess_StatusCode204()
        {
            var httpResponse = await _client.DeleteAsync("api/Users/2");
            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }
        [Fact]
        public async Task DeleteUser_WhenNotExistingUser_StatusCode404()
        {
            var httpResponse = await _client.DeleteAsync("api/Users/25462");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
        [Fact]
        public async Task DeleteTask_WhenSuccess_StatusCode204()
        {
            var httpResponse = await _client.DeleteAsync("api/Tasks/2");
            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenDeletingNotExisted_StatusCode204()
        {
            await _client.DeleteAsync("api/Tasks/3");
            var httpResponse = await _client.DeleteAsync("api/Tasks/3");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CreateTask_WhenSuccess_StatusCode201()
        {
            
            TaskDTO t = new TaskDTO
            {
                CreatedAt = DateTime.UtcNow,
                Name = "Lol",
                FinishedAt = DateTime.Now,
                Description = "agaga",
                State = TaskStateDTO.Started,
                PerformerId = 15,
                ProjectId = 1,
               
            };
            string proj = JsonConvert.SerializeObject(t);
            var httpResponse = await _client.PostAsync("api/Tasks", new StringContent(proj, Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }
        [Fact]
        public async Task CreateTeam_WhenCreted_ThenStatusCode200()
        {
            TeamDTO t = new TeamDTO
            {
                Name = "Lol",
                CreatedAt = DateTime.UtcNow
            };
            string ehhh = JsonConvert.SerializeObject(t);
            var httpResponse = await _client.PostAsync("api/Teams", new StringContent(ehhh, Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }
        [Fact]
        public async Task GetTeamsWithUsersOlderThan10_WhenTeamHasUser_ThenItIsOlderThan10()
        {
            var httpResponse = await _client.GetAsync("api/Teams/GetTeamsWithUsersOlderThan10");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var teams = JsonConvert.DeserializeObject<List<TeamListDTO>>(stringResponse);
            Assert.True(2020 - teams[2].Members[0].Birthday.Year > 10);
        }
        [Fact]
        public async Task GetTeamsWithUsersOlderThan10_WhenTeamHasUser_NotAllTeamsInTheList()
        {
            var httpResponse = await _client.GetAsync("api/Teams/GetTeamsWithUsersOlderThan10");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var teams = JsonConvert.DeserializeObject<List<TeamListDTO>>(stringResponse);
            Assert.NotEqual(10, teams.Count);
        }
        [Fact]
        public async Task GetNotReadyTasksForUser_WhenTaskInList_ThenItIsNotFinished()
        {
            var httpResponse = await _client.GetAsync("api/Tasks/GetNotFinishedTasksForUser/35");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            Assert.NotEqual(TaskStateDTO.Finished, tasks[0].State);
        }
        [Fact]
        public async Task GetNotReadyTasksForUser_WhenOK_ThenStatusCodeOK()
        {
            var httpResponse = await _client.GetAsync("api/Tasks/GetNotFinishedTasksForUser/35");
            Assert.Equal(HttpStatusCode.OK,httpResponse.StatusCode);
        }


    }
}
