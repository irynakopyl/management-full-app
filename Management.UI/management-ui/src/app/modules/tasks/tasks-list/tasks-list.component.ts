import { Component, OnInit } from '@angular/core';

import { TaskModel } from '../../../models/taskModel';
import { TaskService } from '../../../services/task.service';
import { TasksModule } from '../tasks.module';
@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

  tasks: TaskModel[];
  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.getTasks();
  }
  delete(task: TaskModel): void {
    if(confirm("do u really want to delete the task?"))
    {
      this.tasks = this.tasks.filter(h => h !== task);
      this.taskService.deleteTask(task).subscribe();
    }
  }
  getTasks(): void {  
    this.taskService.getTasks()
    .subscribe(tasks => this.tasks = tasks);

  }
}

