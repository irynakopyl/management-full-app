import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserModel } from '../../../models/userModel';
import { UserService }  from '../../../services/user.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  saved: boolean = true;
  

  @Input() user: UserModel = {} as UserModel;
  @Output() userChange = new EventEmitter<UserModel>();
  // editNameHidden = true;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private location: Location
  ) {}
  ngOnInit(): void {
    
  }

  add(user: UserModel): void {
    this.userService.addUser(user)
      .subscribe();
  }
}
