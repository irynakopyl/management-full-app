import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersModule } from './modules/users/users.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { TasksModule } from './modules/tasks/tasks.module';
import { TeamsModule } from './modules/teams/teams.module';
import { ProjectsModule } from './modules/projects/projects.module';

import {ExitGuard} from './guards/is-saved.guard'
import { from } from 'rxjs';
import { TaskStateDirective } from './directives/task-state.directive';
import { DateTraslationPipe } from './pipes/date-traslation.pipe';
@NgModule({
  declarations: [
    AppComponent,
    TaskStateDirective,
    DateTraslationPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    UsersModule,
    TeamsModule,
    ProjectsModule,
    TasksModule,
    DashboardModule, 
  ],
  providers: [ ExitGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
