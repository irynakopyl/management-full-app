using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Tests
{
    public class UserServiceTests
    {
        readonly UserService _userService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public UserServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _userService = new UserService(_uow, _mapper);
        }

        [Fact]
        public async Task AddUser_WhenNewUser_ThenUsersPlusOne()
        {
            var users = await _userService.GetUsers();
            int amount = users.Count;
            var user = new UserDTO
            {
                FirstName = "Lol",
                LastName = "Kekovich",
                Email = "lol@kek.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1,
            };
            await _userService.CreateUser(user);
            users = await _userService.GetUsers();
            Assert.Equal(amount + 1, users.Count);
        }
        [Fact]
        public async Task AddUser_WhenDeleteExistingUser_ThenUsersCountNotChanges()
        {
            var users = await _userService.GetUsers();
            int amount = users.Count;
            var user = new UserDTO
            {
                FirstName = "Lol",
                LastName = "Kekovich",
                Email = "lol@kek.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1,
            };
            user = await _userService.CreateUser(user);
            await _userService.DeleteUser(user.Id);
            var count = await _userService.GetUsers();
            Assert.Equal(amount, count.Count);
        }
        [Fact]
        public async Task UpdateUserToTeam_WhenNewTeam_ThenNewTeamId()
        {
            var uzrs =await  _userService.GetUsers();
            UserDTO usr = uzrs.FirstOrDefault();
            usr.TeamId = 10;
            UserDTO lol = new UserDTO
            {
                Birthday = usr.Birthday,
                Email = usr.Email,
                FirstName = usr.FirstName,
                LastName = usr.LastName,
                RegisteredAt = usr.RegisteredAt,
                TeamId = usr.TeamId
            };
            await _userService.UpdateUser(usr);
            var user = await _userService.GetUserById(usr.Id);
            Assert.Equal(10, user.TeamId);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(55)]
        public async Task GetTaskAmountInProjects_EqualsProjectAmount(int id)
        {
            var tasks = await _userService.GetTaskAmountInProjects(id);
            Assert.Equal(100, tasks.Count);
        }

        [Theory]
        [InlineData(15)]
        public async Task GetAllTasksFinishedInCurrentYear_WhenUser15_EqualsTwo(int id)
        {
            var finished = await _userService.GetAllTasksFinishedInCurrentYear(id);
            Assert.Equal(2, finished.Count);
        }
        [Fact]
        public async Task GetAllSortedUsersWithSortedTasks_AllUsersReturned()
        {
            var users = await _userService.GetUsers();
            var sortedUsers = await _userService.GetAllSortedUsersWithSortedTasks();
            Assert.Equal(users.Count, sortedUsers.Count);
        }
        [Theory]
        [InlineData(23)]
        [InlineData(37)]
        public async Task GetAllSortedUsersWithSortedTasks_WhenSorted_TaskLengthAreGreater(int id)
        {
            var sortedUsers = await _userService.GetAllSortedUsersWithSortedTasks();
            var task1 = sortedUsers.ToList()[id].Tasks[1].Name.Length;
            var task2 = sortedUsers.ToList()[id].Tasks[2].Name.Length;
            Assert.True(task1 >= task2);
        }
        [Theory]
        [InlineData(19)]
        public async Task GetTasksAndProjectsInfo(int id)
        {
            var taski =await  _userService.GetTasksAndProjectsInfo(id);
            Assert.Equal(id, taski.User.Id);
        }

    }
}
