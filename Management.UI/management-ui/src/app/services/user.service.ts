import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { UserModel } from '../models/userModel';
import { USERS } from '../services/usersMock';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl = 'https://localhost:44305/api/Users';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient
    ) { }
  
  getUsers(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(this.usersUrl)
    .pipe(
      catchError(this.handleError<UserModel[]>('getUsers', [])
    ));
  }

  getUser(id: number): Observable<UserModel> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<UserModel>(url).pipe(
      catchError(this.handleError<UserModel>(`getHero id=${id}`))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  /** PUT: update the hero on the server */
    updateUser(user: UserModel): Observable<any> {
      console.log('user updated: '+ user.firstName)
    return this.http.put(this.usersUrl, user, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser(user: UserModel): Observable<any> {
    console.log('user deleted: '+ user.firstName)
  return this.http.delete(this.usersUrl+'/'+ user.id).pipe(
    catchError(this.handleError<any>('updateUser'))
  );
  }
  addUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(this.usersUrl, user, this.httpOptions).pipe(
      catchError(this.handleError<UserModel>('addUser'))
    );
  }
}

