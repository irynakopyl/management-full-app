import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectCreateComponent } from './project-create/project-create.component';



@NgModule({
  declarations: [ProjectListComponent, ProjectDetailComponent, ProjectCreateComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [ProjectListComponent, ProjectDetailComponent, ProjectCreateComponent]
})
export class ProjectsModule { }
