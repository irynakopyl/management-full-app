import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TeamModel } from '../../../models/teamsModel';
import { TeamService }  from '../../../services/team.service';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit {
  saved: boolean = true;

  constructor( private route: ActivatedRoute,
    private teamService: TeamService,
    private location: Location) { }

  @Input() team: TeamModel = {} as TeamModel;
  @Output() teamChange = new EventEmitter<TeamModel>();

  ngOnInit(): void {
  }

  add(team: TeamModel): void {
    this.teamService.addTeam(team)
      .subscribe(() => this.goBack());
  }
  goBack(): void {
    this.location.back();
  }
}


