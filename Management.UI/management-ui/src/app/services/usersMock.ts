import { UserModel } from '../models/userModel';
export const USERS: UserModel[] = [
    {
        id :1,
        firstName: 'Lol',
        lastName: 'Kek',
        email: 'lol@kek.com',
        birthday:  new Date() ,
        registeredAt: new Date(),
        teamId: 1
      },
      {
        id :2,
        firstName: 'O',
        lastName: 'Hara',
        email: 'My@mail.com',
        birthday:  new Date() ,
        registeredAt: new Date(),
        teamId: 2
      },
      {
        id :3,
        firstName: 'Maki',
        lastName: 'Posne',
        email: 'My@mail.com',
        birthday:  new Date() ,
        registeredAt: new Date(),
        teamId: 2
      }
]