export interface TeamModel {
    id: number;
    name : string;
    createdAt: Date;
}