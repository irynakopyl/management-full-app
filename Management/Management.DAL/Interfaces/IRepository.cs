﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Management.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> Get(int id);
        Task<IEnumerable<TEntity>> Find(Func<TEntity, Boolean> predicate);
        Task Create(TEntity item);
        Task Update(TEntity item);
        Task Delete(TEntity entity);
    }
}
