﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.Common.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TeamDTO team)
        {
            if (ModelState.IsValid)
            {
                return Created("Post", await _teamService.CreateTeam(team));
            }
            return NotFound();
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {
            return Ok(await _teamService.GetTeamById(id));
        }

        [Route("GetTeamsWithUsersOlderThan10")]
        public async Task<ActionResult<ICollection<TeamListDTO>>> GetTeamsWithUsersOlderThan10()
        {
            return Ok( await _teamService.GetTeamsWithUsersOlderThan10());
        }

        [HttpPut]
        public async Task<ActionResult<TeamDTO>> Put([FromBody] TeamDTO team)
        {
            await _teamService.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _teamService.DeleteTeam(id);
                return NoContent();
            }
            catch { return NotFound(); }
        }
    }
}
