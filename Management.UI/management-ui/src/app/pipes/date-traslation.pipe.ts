import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import localeUa from '@angular/common/locales/uk';
import { registerLocaleData } from '@angular/common';

registerLocaleData(localeUa, 'ua');

@Pipe({
  name: 'dateTraslation'
})
export class DateTraslationPipe extends DatePipe implements PipeTransform {

  transform(value: Date, args?: any): any {
    if (value) {
      return super.transform(value,'longDate', '+0200','ua');
   }
   return value;
  }

}
