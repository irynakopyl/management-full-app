import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TaskModel } from '../../../models/taskModel';
import { TaskService }  from '../../../services/task.service';
import { TaskState } from '../../../models/taskModel';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {

  saved: boolean = true;
  @Input() task: TaskModel = {} as TaskModel;
  @Output() taskChange = new EventEmitter<TaskModel>();
  // editNameHidden = true;
  state = TaskState;
  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private location: Location
  ) {}
  ngOnInit(): void {
    
  }

  add(task: TaskModel): void {
    this.taskService.addTask(task)
      .subscribe(()=>this.goBack());
  }
  goBack(): void {
    this.location.back();
  }
}

