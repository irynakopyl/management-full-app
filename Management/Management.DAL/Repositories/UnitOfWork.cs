﻿using Management.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Models;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Management.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;
        public IRepository<User> UsersRepo { get; set; }
        public IRepository<Team> TeamsRepo { get; set; }
        public IRepository<Project> ProjectsRepo { get; set; }
        public IRepository<Taska> TasksRepo { get; set; }
        private readonly ManagementDbContext _context;
        public UnitOfWork(ManagementDbContext context)
        {   
            UsersRepo = new UserRepository(context);
            TeamsRepo = new TeamRepository(context);
            ProjectsRepo = new ProjectRepository(context);
            TasksRepo = new TaskaRepository(context);
            _context = context;
        }
        
        public void Save()
        {
            _context.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
