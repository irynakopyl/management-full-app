import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location, DatePipe } from '@angular/common';
import { TeamModel } from '../../../models/teamsModel';
import { TeamService }  from '../../../services/team.service';
import localeUa from '@angular/common/locales/uk';
import { registerLocaleData } from '@angular/common';

import { Observable } from 'rxjs';

registerLocaleData(localeUa, 'ua');

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {
  saved: boolean = true;

  constructor(private route: ActivatedRoute,
    private teamService: TeamService,
    private location: Location, private datePipe: DatePipe) { }

  @Input() team: TeamModel = {} as TeamModel;
  @Output() teamChange = new EventEmitter<TeamModel>();

  ngOnInit(): void {
    this.getTeam();

  }
  getTeam(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.teamService.getTeam(id)
      .subscribe(team => this.team = team);
  }
  save(): void {
    this.saved = true;
    console.log('SAVESD');
    this.teamChange.emit(this.team);
    this.teamService.updateTeam(this.team)
      .subscribe(() => this.goBack());
  }
  canDeactivate() : boolean | Observable<boolean>{
    if(!this.saved){
        return confirm("You have unsaved data! do u want to leave the page?");
    }
    else{
        return true;
    }
}
  goBack(): void {
    this.location.back();
  }
}

  


