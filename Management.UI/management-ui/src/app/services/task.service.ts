import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TaskModel } from '../models/taskModel';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private tasksUrl = 'https://localhost:44305/api/Tasks';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient
    ) { }
  
  getTasks(): Observable<TaskModel[]> {
    return this.http.get<TaskModel[]>(this.tasksUrl)
    .pipe(
      catchError(this.handleError<TaskModel[]>('getTasks', [])
    ));
  }

  getTask(id: number): Observable<TaskModel> {
    const url = `${this.tasksUrl}/${id}`;
    return this.http.get<TaskModel>(url).pipe(
      catchError(this.handleError<TaskModel>(`getTask id=${id}`))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
    updateTask(Task: TaskModel): Observable<any> {
      console.log('Task updated: '+ Task.name)
    return this.http.put(this.tasksUrl, Task, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateTask'))
    );
  }

  deleteTask(Task: TaskModel): Observable<any> {
    console.log('Task deleted: '+ Task.name)
  return this.http.delete(this.tasksUrl+'/'+ Task.id).pipe(
    catchError(this.handleError<any>('updateTask'))
  );
  }
  addTask(Task: TaskModel): Observable<TaskModel> {
    return this.http.post<TaskModel>(this.tasksUrl, Task, this.httpOptions).pipe(
      catchError(this.handleError<TaskModel>('addTask'))
    );
  }
}

