import { Component, OnInit } from '@angular/core';

import { ProjectModel } from '../../../models/projectModel';
import { ProjectService } from '../../../services/project.service';
import { ProjectsModule } from '../projects.module';
@Component({
  selector: 'app-projects-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  projects: ProjectModel[];
  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.getProjects();
  }
  delete(project: ProjectModel): void {
    if(confirm("do u really want to delete the project?"))
    {
      this.projects = this.projects.filter(h => h !== project);
      this.projectService.deleteProject(project).subscribe();
    }
  }
  getProjects(): void {  
    this.projectService.getProjects()
    .subscribe(projects => this.projects = projects);

  }
}

