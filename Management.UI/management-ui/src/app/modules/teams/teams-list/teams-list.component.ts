import { Component, OnInit } from '@angular/core';

import { TeamModel } from '../../../models/teamsModel';
import { TeamService } from '../../../services/team.service';
import { TeamsModule } from '../teams.module';
@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {

  teams: TeamModel[];
  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
    this.getTeams();
  }
  delete(team: TeamModel): void {
    if(confirm("do u really want to delete the team?"))
    {
      this.teams = this.teams.filter(h => h !== team);
      this.teamService.deleteTeam(team).subscribe();
    }
  }
  getTeams(): void {  
    this.teamService.getTeams()
    .subscribe(teams => this.teams = teams);

  }
}

