import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TaskModel, TaskState } from '../../../models/taskModel';
import { TaskService }  from '../../../services/task.service';
import localeUa from '@angular/common/locales/uk';
import { registerLocaleData } from '@angular/common';

import { Observable } from 'rxjs';

registerLocaleData(localeUa, 'ua');

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {
  saved: boolean = true;
  color: string = '';
  constructor(private route: ActivatedRoute,
    private taskService: TaskService,
    private location: Location) { }

  @Input() task: TaskModel = {} as TaskModel;
  @Output() taskChange = new EventEmitter<TaskModel>();
    state = TaskState;
  ngOnInit(): void {
    this.getTask();

  }
  getTask(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.taskService.getTask(id)
      .subscribe(task => this.task = task);
  }
  save(): void {
    this.saved = true;
    console.log('SAVESD');
    this.taskChange.emit(this.task);
    this.taskService.updateTask(this.task)
      .subscribe(() => this.goBack());
  }
  canDeactivate() : boolean | Observable<boolean>{
    if(!this.saved){
        return confirm("You have unsaved data! do u want to leave the page?");
    }
    else{
        return true;
    }
}
  goBack(): void {
    this.location.back();
  }
}

  
