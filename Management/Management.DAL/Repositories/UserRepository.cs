﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ManagementDbContext _dataContext;
        public UserRepository(ManagementDbContext data)
        {
            _dataContext = data;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await Task.Run(() => _dataContext.Users.AsNoTracking());
        }

        public async Task<User> Get(int id)
        {
            return await Task.Run(() => _dataContext.Users.Find(id));
        }

        public async Task Create(User entity)
        {
            await Task.Run(() => _dataContext.Users.Add(entity));
        }

        public async Task Update(User entity)
        {
            await Task.Run(() => _dataContext.Entry(entity).State = EntityState.Modified);

        }

        public async Task<IEnumerable<User>> Find(Func<User, Boolean> predicate)
        {
            return await Task.Run(() => _dataContext.Users.Where(predicate).ToList());
        }

        public async Task Delete(User entity)
        {
            User user = await Task.Run(() => _dataContext.Users.Find(entity.Id));
            if (user != null)
                await Task.Run(() => _dataContext.Users.Remove(user));
        }
    }
}
