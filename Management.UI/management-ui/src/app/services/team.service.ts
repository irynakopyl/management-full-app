import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TeamModel } from '../models/teamsModel';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private teamsUrl = 'https://localhost:44305/api/Teams';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient
    ) { }
  
  getTeams(): Observable<TeamModel[]> {
    return this.http.get<TeamModel[]>(this.teamsUrl)
    .pipe(
      catchError(this.handleError<TeamModel[]>('getTeams', [])
    ));
  }

  getTeam(id: number): Observable<TeamModel> {
    const url = `${this.teamsUrl}/${id}`;
    return this.http.get<TeamModel>(url).pipe(
      catchError(this.handleError<TeamModel>(`getTeam id=${id}`))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
    updateTeam(team: TeamModel): Observable<any> {
      console.log('team updated: '+ team.name)
    return this.http.put(this.teamsUrl, team, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateTeam'))
    );
  }

  deleteTeam(team: TeamModel): Observable<any> {
    console.log('team deleted: '+ team.name)
  return this.http.delete(this.teamsUrl+'/'+ team.id).pipe(
    catchError(this.handleError<any>('updateTeam'))
  );
  }
  addTeam(team: TeamModel): Observable<TeamModel> {
    return this.http.post<TeamModel>(this.teamsUrl, team, this.httpOptions).pipe(
      catchError(this.handleError<TeamModel>('addTeam'))
    );
  }
}

