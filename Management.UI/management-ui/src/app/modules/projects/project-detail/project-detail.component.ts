import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProjectModel } from '../../../models/projectModel';
import { ProjectService }  from '../../../services/project.service';
import localeUa from '@angular/common/locales/uk';
import { registerLocaleData } from '@angular/common';

import { Observable } from 'rxjs';

registerLocaleData(localeUa, 'ua');

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  saved: boolean = true;
  
  constructor(private route: ActivatedRoute,
    private projectService: ProjectService,
    private location: Location) { }

  @Input() project: ProjectModel = {} as ProjectModel;
  @Output() projectChange = new EventEmitter<ProjectModel>();
  ngOnInit(): void {
    this.getProject();

  }
  getProject(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.projectService.getProject(id)
      .subscribe(project => this.project = project);
  }
  save(): void {
    this.saved = true;
    console.log('SAVESD');
    this.projectChange.emit(this.project);
    this.projectService.updateProject(this.project)
      .subscribe(() => this.goBack());
  }
  canDeactivate() : boolean | Observable<boolean>{
    if(!this.saved){
        return confirm("You have unsaved data! do u want to leave the page?");
    }
    else{
        return true;
    }
}
  goBack(): void {
    this.location.back();
  }
}

  
