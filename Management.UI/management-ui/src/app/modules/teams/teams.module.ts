import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DateTraslationPipe } from '../../pipes/date-traslation.pipe';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { TeamCreateComponent } from './team-create/team-create.component';

@NgModule({
  declarations: [TeamsListComponent, TeamDetailComponent, TeamCreateComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  providers:[DateTraslationPipe],
  exports: [
    TeamsListComponent,
    TeamDetailComponent,
    TeamCreateComponent
  ]
})
export class TeamsModule { }
