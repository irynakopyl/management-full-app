﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.BLL.Services.Abstract;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {

        }
        public async Task<UserDTO> CreateUser(UserDTO newUser)
        {
            var userEntity = _mapper.Map<User>(newUser);
           await _context.UsersRepo.Create(userEntity);
            await _context.SaveAsync();
            var createdTask = await _context.UsersRepo.Get(userEntity.Id);
            return  _mapper.Map<UserDTO>(createdTask);
        }
        public async  Task<ICollection<UserDTO>> GetUsers()
        {
            var teams = await _context.UsersRepo.GetAll();
            return _mapper.Map<ICollection<UserDTO>>(teams.ToList());
        }
        public async Task<UserDTO> GetUserById(int id)
        {
            User user = await _context.UsersRepo.Get(id);
            return _mapper.Map<UserDTO>(user);
        }
        public async Task<UserDTO> UpdateUser(UserDTO user)
        {
            var result = _mapper.Map<User>(user);
            await _context.UsersRepo.Update(result);
            await _context.SaveAsync();
            var usr = await _context.UsersRepo.Get(result.Id);
            return _mapper.Map<UserDTO>(usr);
        }
        public async Task DeleteUser(int id)
        {
            var usr = await _context.UsersRepo.Get(id);
            if (usr == null) throw new ArgumentException("Invalid id");
            await _context.UsersRepo.Delete(usr);
            await _context.SaveAsync();
        }
        public async Task<IDictionary<string, int>> GetTaskAmountInProjects(int userId)
        {
            var tasksList = await _context.TasksRepo.GetAll();
            var projList = await _context.ProjectsRepo.GetAll();

            var projects = _mapper.Map<ICollection<ProjectDTO>>(projList);
            var tasks = _mapper.Map<ICollection<TaskDTO>>(tasksList);

            return projects.GroupJoin(tasks,
                p => p.Id,
                t => t.ProjectId,
                (pr, task) =>
                new
                {
                    p = pr.Name,
                    t = task.Where(
                    x => x.PerformerId == userId).ToList().Count
                }
                ).ToDictionary(p =>p.p, p => p.t);
        }
        public async Task<ICollection<ReadyTaskInCurrentYearDTO>> GetAllTasksFinishedInCurrentYear(int userId)
        {
            var projList = await _context.TasksRepo.GetAll();
            IEnumerable<TaskDTO> tasks = _mapper.Map<IEnumerable<TaskDTO>>(projList);
            return tasks.Where(x => x.State == TaskStateDTO.Finished && x.FinishedAt.Year == 2020 && x.PerformerId == userId)
                .Select(t =>
               new ReadyTaskInCurrentYearDTO
               {
                   Id = t.Id,
                   Name = t.Name
               }).ToList();

        }
        public async Task<ICollection<TasksListForUserDTO>> GetAllSortedUsersWithSortedTasks()
        {
            var tasksList = await _context.TasksRepo.GetAll();
            var usersList = await _context.UsersRepo.GetAll();

            IEnumerable<TaskDTO> tasks = _mapper.Map<IEnumerable<TaskDTO>>(tasksList);
            IEnumerable<UserDTO> uzzers = _mapper.Map<IEnumerable<UserDTO>>(usersList);
            return uzzers.GroupJoin(tasks,
                 user => user.Id,
                 task => task.PerformerId,
                (t, u) =>  new TasksListForUserDTO
                 {
                     Performer = t,
                     Tasks = u.OrderByDescending(t => t.Name.Length).ToList()
                 }
                ).OrderBy(t => t.Performer.FirstName).ToList();
        }
        public async Task<UserInfoDTO> GetTasksAndProjectsInfo(int userId)
        {
            UserInfoDTO user = new UserInfoDTO() { };
            var usr = await _context.UsersRepo.Get(userId);
            var tasksList = await _context.TasksRepo.GetAll();
            var projList = await _context.ProjectsRepo.GetAll();

            user.User = _mapper.Map<UserDTO>(usr);
            user.LastProject = _mapper.Map<ICollection<ProjectDTO>>(projList).Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefault();//* Останній проект користувача(за датою створення)
            user.TaskAmountUnderLastProject = _mapper.Map<ICollection<TaskDTO>>(tasksList).Where(p => p.ProjectId == user.LastProject.Id).ToList().Count;//* Загальна кількість тасків під останнім проектом
            user.LongestTask = _mapper.Map<ICollection<TaskDTO>>(tasksList).Where(p => p.PerformerId == userId) //* Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
               .Distinct()
               .OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault();

            user.NotReadyTasks = _mapper.Map<ICollection<ProjectDTO>>(projList).SelectMany(p => p.Tasks.Where(
                p => (p.PerformerId == userId && (p.State == TaskStateDTO.Canceled || p.State == TaskStateDTO.Started)))).Distinct().ToList().Count;//* Загальна кількість незавершених або скасованих тасків для користувача
            return user;

        }
    }
}
