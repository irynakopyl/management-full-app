import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersListComponent } from './modules/users/users-list/users-list.component';
import { UserDetailComponent }  from './modules/users/user-detail/user-detail.component';
import { UserCreateComponent }  from './modules/users/user-create/user-create.component';

import { DashboardComponent } from './modules/dashboard/dashboard/dashboard.component';

import { TeamsListComponent } from './modules/teams/teams-list/teams-list.component';
import { TeamDetailComponent } from './modules/teams/team-detail/team-detail.component';
import { TeamCreateComponent } from './modules/teams/team-create/team-create.component';

import { TasksListComponent } from './modules/tasks/tasks-list/tasks-list.component';
import { TaskDetailComponent } from './modules/tasks/task-detail/task-detail.component';
import { TaskCreateComponent } from './modules/tasks/task-create/task-create.component';

import { ProjectListComponent } from './modules/projects/project-list/project-list.component';
import { ProjectDetailComponent } from './modules/projects/project-detail/project-detail.component';
import { ProjectCreateComponent } from './modules/projects/project-create/project-create.component';

import { ExitGuard } from './guards/is-saved.guard';
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'users/:id', component: UserDetailComponent, canDeactivate:[ExitGuard] },
  { path: 'users', component: UsersListComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'newUser', component: UserCreateComponent},

  { path: 'teams/:id', component: TeamDetailComponent, canDeactivate:[ExitGuard] },
  { path: 'teams', component: TeamsListComponent },
  { path: 'newTeam', component: TeamCreateComponent},

  { path: 'tasks/:id', component: TaskDetailComponent, canDeactivate:[ExitGuard] },
  { path: 'tasks', component: TasksListComponent },
  { path: 'newTask', component: TaskCreateComponent},

  
  { path: 'projects/:id', component: ProjectDetailComponent, canDeactivate:[ExitGuard] },
  { path: 'projects', component: ProjectListComponent },
  { path: 'newProject', component: ProjectCreateComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
