import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ProjectModel } from '../models/projectModel';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectsUrl = 'https://localhost:44305/api/Projects';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient
    ) { }
  
  getProjects(): Observable<ProjectModel[]> {
    return this.http.get<ProjectModel[]>(this.projectsUrl)
    .pipe(
      catchError(this.handleError<ProjectModel[]>('getProjects', [])
    ));
  }

  getProject(id: number): Observable<ProjectModel> {
    const url = `${this.projectsUrl}/${id}`;
    return this.http.get<ProjectModel>(url).pipe(
      catchError(this.handleError<ProjectModel>(`getProject id=${id}`))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
    updateProject(Project: ProjectModel): Observable<any> {
      console.log('Project updated: '+ Project.name)
    return this.http.put(this.projectsUrl, Project, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateProject'))
    );
  }

  deleteProject(Project: ProjectModel): Observable<any> {
    console.log('Project deleted: '+ Project.name)
  return this.http.delete(this.projectsUrl+'/'+ Project.id).pipe(
    catchError(this.handleError<any>('updateProject'))
  );
  }
  addProject(Project: ProjectModel): Observable<ProjectModel> {
    return this.http.post<ProjectModel>(this.projectsUrl, Project, this.httpOptions).pipe(
      catchError(this.handleError<ProjectModel>('addProject'))
    );
  }
}

