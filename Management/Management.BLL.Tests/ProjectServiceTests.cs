﻿using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Tests
{
    public class ProjectServiceTests
    {
        readonly ProjectService _projectService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public ProjectServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _projectService = new ProjectService(_uow, _mapper);
        }
        [Fact]
        public async Task GetAllProjectsAndItsTasksInfo_ThenAllProjects()
        {
            var res = await _projectService.GetAllProjectsAndItsTasksInfo();
            Assert.Equal(100, res.Count);
        }
        [Fact]
        public async Task GetAllProjectsAndItsTasksInfo_WhenProjectNameMoreThan20_ThenUsersInTeamNotZero()
        {
            var res = await _projectService.GetAllProjectsAndItsTasksInfo();
            Assert.NotEqual(0, res.ToList()[20].UserAmount);
        }
    }
}
