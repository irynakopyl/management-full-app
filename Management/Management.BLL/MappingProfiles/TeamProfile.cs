﻿using Management.Common.DTO;
using Management.DAL.Models;
using AutoMapper;

namespace Management.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
