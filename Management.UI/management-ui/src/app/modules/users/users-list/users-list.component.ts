import { Component, OnInit } from '@angular/core';

import { UserModel } from '../../../models/userModel';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  usrs : UserModel[];
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }
  delete(user: UserModel): void {
    if(confirm("do u really want to delete the user?"))
    {
      this.usrs = this.usrs.filter(h => h !== user);
      this.userService.deleteUser(user).subscribe();
    }
  }
  getUsers(): void {  
    this.userService.getUsers()
    .subscribe(users => this.usrs = users);

  }
}
