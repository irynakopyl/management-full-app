﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class TaskaRepository : IRepository<Taska>
    {
        private readonly ManagementDbContext _dataContext;
        public TaskaRepository(ManagementDbContext context)
        {
            _dataContext = context;
        }

        public async Task<IEnumerable<Taska>> GetAll()
        {
            return await Task.Run(() => _dataContext.Tasks.AsNoTracking()); 
        }

        public async Task<Taska> Get(int id)
        {
            return await Task.Run(() => _dataContext.Tasks.Find(id));
        }

        public async Task Create(Taska entity)
        {
            await Task.Run(() => _dataContext.Tasks.Add(entity));
        }

        public async Task Update(Taska entity)
        {
            await Task.Run(() => _dataContext.Entry(entity).State = EntityState.Modified);
        }
        public async Task<IEnumerable<Taska>> Find(Func<Taska, Boolean> predicate)
        {
            return await Task.Run(() => _dataContext.Tasks.Where(predicate));
        }

        public async Task Delete(Taska entity)
        {
            Taska task = await Task.Run(()=>_dataContext.Tasks.Find(entity.Id));
            if (task != null)
                await Task.Run(() => _dataContext.Tasks.Remove(task));
        }
    }
}
