import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserModel } from '../../../models/userModel';
import { UserService }  from '../../../services/user.service';
import localeUa from '@angular/common/locales/uk';
import { registerLocaleData } from '@angular/common';

import { Observable } from 'rxjs';
registerLocaleData(localeUa, 'ua');

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: [ './user-detail.component.css' ]
})
export class UserDetailComponent implements OnInit {
  saved: boolean = true;
  

  @Input() user: UserModel = {} as UserModel;
  @Output() userChange = new EventEmitter<UserModel>();
  // editNameHidden = true;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getUser();
  }

  
  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUser(id)
      .subscribe(usr => this.user = usr);
  }
  save(): void {
    this.saved = true;
    console.log('SAVESD');
    this.userChange.emit(this.user);
    this.userService.updateUser(this.user)
      .subscribe(() => this.goBack());
  }
  canDeactivate() : boolean | Observable<boolean>{
    if(!this.saved){
        return confirm("You have unsaved data! do u want to leave the page?");
    }
    else{
        return true;
    }
}
  goBack(): void {
    this.location.back();
  }
}
