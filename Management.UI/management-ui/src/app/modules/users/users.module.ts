import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UsersListComponent } from './users-list/users-list.component';
import { UserDetailComponent } from '../users/user-detail/user-detail.component';
import { UserCreateComponent } from './user-create/user-create.component'
@NgModule({
  declarations: [UsersListComponent, UserDetailComponent,  UserCreateComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    UsersListComponent,
    UserDetailComponent,
    UserCreateComponent
  ]
})
export class UsersModule { }
