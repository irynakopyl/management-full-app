import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskCreateComponent } from './task-create/task-create.component';



@NgModule({
  declarations: [TasksListComponent, TaskDetailComponent, TaskCreateComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [TasksListComponent, TaskDetailComponent, TaskCreateComponent]
})
export class TasksModule { }
