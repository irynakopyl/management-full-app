export interface TaskModel {
    id: number;
    name : string;
    description: string;
    taskState: TaskState;
    createdAt: Date;
    finishedAt: Date;
    projectId?: number; 
    performerId?: number;
  }
export enum TaskState
{
    Created = 0,
    Started = 1,
    Finished = 2,
    Canceled = 3
}     
        