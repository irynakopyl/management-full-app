import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProjectModel } from '../../../models/projectModel';
import { ProjectService }  from '../../../services/project.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {

  saved: boolean = true;
  @Input() project: ProjectModel = {} as ProjectModel;
  @Output() projectChange = new EventEmitter<ProjectModel>();
 
  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private location: Location
  ) {}
  ngOnInit(): void {
    
  }

  add(project: ProjectModel): void {
    this.projectService.addProject(project)
      .subscribe(()=>this.goBack());
  }
  goBack(): void {
    this.location.back();
  }
}

